Opdracht 2e examenkans: Open the gates for data!
==========================
Productiedossier
--------------------
#####2MMPb : Ribbens Niels
- - -
###Briefing

Het doel van dit project is het ontwikkelen van een eenvoudige applicatie voor zowel mobiele als desktop-apparaten die open data visueleren voor de gebruiker.

Dit gebeurt door een keuze-menu waarbij de gebruiker keuze kan maken uit 10 verschillende datasets van http://data.gent.be/datasets, http://datatank4.gent.be/, http://appsforghent.be/

---
###Analyse

De applicatie wordt zo eenvoudig en gebruiksvriendelijk mogelijk gehouden. De algemene look en beschikbare functies blijven gelijk op de mobiele en desktop-versie. Dit zorgt voor vlot, intuïtief gebruik en houdt de instapdrempel laag. Er werd gebruik gemaakt van een minimalistisch, maar functioneel kleurenpallet. Iedere dataset wordt weergegeven in een lijstweergave en een kaartweergave.

---
###Technische Specificaties

Frontend (One Page Applicatie)
	HTML5, CSS3 en JavaScript
	jQuery, underscore.js, lodash.js, crossroads.js, js-signals
	localstorage of IndexedDB
Componenten worden via Bower toegevoegd in de components folder van de app folder
SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
CSS-bestanden worden met elkaar verbonden in één bestand en geminified
De JS code wordt automatisch nagekeken op syntax fouten
JS-bestanden worden met elkaar verbonden in één bestand en geminified
De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken

---
###Functionele Specificaties

Verplichte features:

Gebruik minimaal 10 datasets uit de websites: http://data.gent.be/datasets, http://datatank4.gent.be/, http://appsforghent.be/
* 1.  Losloopweides: http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json
* 2.  Parkeergarages: http://datatank.gent.be/Infrastructuur/Parkeergarages.json
* 3.  Buurtcentra: http://datatank.gent.be/Infrastructuur/Buurtcentra.json
* 4.  Bibliotheken: http://datatank4.gent.be/cultuursportvrijetijd/bibliotheek.json
* 5.  Bioscopen: http://datatank4.gent.be/cultuursportvrijetijd/bioscopen.json
* 6.  Parken: http://datatank4.gent.be/milieuennatuur/parken.json
* 7.  Speelstraten: http://datatank4.gent.be/cultuursportvrijetijd/speelstratenzomer2015.json
* 8.  Ziekenhuizen: http://data.appsforghent.be/poi/ziekenhuizen.json
* 9.  Huisartsen: http://data.appsforghent.be/poi/huisartsenwachtposten.json
* 10. Apotheken: http://data.appsforghent.be/poi/apotheken.json
Data visualisaties
Geolocatie
Google Maps / Leaflet

---
###Persona's

#####David
![David](https://lh4.googleusercontent.com/OTYDQyrfBHQRUJJA_8IdtmmwYQKcB6uUM1w0QhJloA=s200 "David.jpg")

David is 32 jaar en is manager bij een middelgrote onderneming.
Hij is getrouwd en heeft 2 kinderen. David is erg sportief en
voetbalt graag, in de week speelt hij competitief minivoetbal.
David is woonachtig te Gent en heeft daarnaast nog een hond 
waar heel wat tijd in kruipt. David wil weten waar hij zowel met
zijn kinderen naartoe kan op zijn vrije dagen voor de speelstraten
als ook welke losloopweides in de buurt zijn voor zijn hond.

#####Matthias
![Matthias](https://lh3.googleusercontent.com/W4oOyb6Cblvy7bHEKa1NV6yCw5uHzxfDZQ8w5RA7kQ=s200 "Matthias.jpg")

Matthias is 19 jaar en student Elektromechnica. Hij is afkomstig
uit het Oost-Vlaamse Lokeren en studeert in Gent. Matthias is
naast zijn studie bezig met voetbal en uitgaan, vooral in de
Gentse Overpoort. In de week zit Matthias op kot in Gent, vlak bij
de Overpoortstraat, en in het weekend slaapt hij in het ouderlijk
huis te Lokeren. Matthias ontspant tijdens de week al graag eens met
een bezoek te brengen aan de bioscoop en er is meer dan alleen de Kinepolis.
Daarnaast bezoekt Matthias ook al eens bibliotheken op om te studeren omdat 
hij op kot al eens snel afgeleid raakt.

---
###Moodboard
zie productiedossier.pdf
---
###Sitemap
zie productiedossier.pdf
---
###Wireframes
zie productiedossier.pdf