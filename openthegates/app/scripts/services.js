/**
* Description: Ghent - WHERETOGO application
* 1.  Losloopweides: http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json
* 2.  Parkeergarages: http://datatank.gent.be/Infrastructuur/Parkeergarages.json
* 3.  Buurtcentra: http://datatank.gent.be/Infrastructuur/Buurtcentra.json
* 4.  Bibliotheken: http://datatank4.gent.be/cultuursportvrijetijd/bibliotheek.json
* 5.  Bioscopen: http://datatank4.gent.be/cultuursportvrijetijd/bioscopen.json
* 6.  Parken: http://datatank4.gent.be/milieuennatuur/parken.json
* 7.  Speelstraten: http://datatank4.gent.be/cultuursportvrijetijd/speelstratenzomer2015.json
* 8.  Ziekenhuizen: http://data.appsforghent.be/poi/ziekenhuizen.json
* 9.  Huisartsen: http://data.appsforghent.be/poi/huisartsenwachtposten.json
* 10. Apotheken: http://data.appsforghent.be/poi/apotheken.json
*
*
*/

//Losloopweide
var HondenvoorzieningenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getHondenvoorzieningen:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};


//Parkeergarages
var ParkeergaragesAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getParkeergarages:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Buurtcentra
var BuurtcentraAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getBuurtcentra:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Bibliotheek
var BibliotheekAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getBibliotheek:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Bioscopen
var BioscopenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getBioscopen:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Parken
var ParkenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getParken:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Speelstratenzomer2015
var Speelstratenzomer2015API = {
    init:function(connString){
        this.connString = connString;
    },
    getSpeelstratenzomer2015:function(amount){
        var url = this.connString;

        // debugging
        var json = Utils.getJSONByPromise(url);
        console.log(json);

        return Utils.getJSONByPromise(url);
    }
};

//Ziekenhuizen
//var ZiekenhuizenAPI = {
//    init:function(connString){
//        this.connString = connString;
//    },
//    getZiekenhuizen:function(amount){
//        var url = this.connString;
//
//        // debugging
//        var json = Utils.getJSONByPromise(url);
//        console.log(json);
//
//        return Utils.getJSONByPromise(url);
//    }
//};

//Dokters
//var HuisartsenwachtpostenAPI = {
//    init:function(connString){
//        this.connString = connString;
//    },
//    getHuisartsenwachtposten:function(amount){
//        var url = this.connString;
//
//        // debugging
//        var json = Utils.getJSONByPromise(url);
//        console.log(json);
//
//        return Utils.getJSONByPromise(url);
//    }
//};

//Apotheken
//var ApothekenAPI = {
//    init:function(connString){
//        this.connString = connString;
//    },
//    getApotheken:function(amount){
//        var url = this.connString;
//
//        // debugging
//        var json = Utils.getJSONByPromise(url);
//        console.log(json);
//
//        return Utils.getJSONByPromise(url);
//    }
//};

var WheretogoDBContext = {
    init:function(connString){
        this.connString = connString;
        //Create the AppData object for the Users Application
        this.AppData = {
            "information":{
                "title":"Where To Go Application Ghent",
                "version":"1.0",
                "modified":"01-08-2015",
                "author":"Niels Ribbens"
            },
            "runningplaces":[],
			"parkingspots":[],
			"©":[],
			"libraries":[],
			"movietheatres":[],
            "forest":[],
			"playstreets":[],
			"hospitals":[],
			"doctors":[],
			"pharmacies":[],
            "settings":{}
        };
        //Get Application Data from the localstorage
        if(Utils.store(this.connString) != null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
 
 //get...
    getRunningPlaces:function(){
        var runningplaces = this.AppData.runningplaces;

        if(runningplaces == null || (runningplaces != null && runningplaces.length == 0))
            return null;

        runningplaces = _.sortBy(runningplaces,'guid');
        return {"runningplaces":runningplaces};
    },
   
   getParkingSpots:function(){
        var parkingspots = this.AppData.parkingspots;

        if(parkingspots == null || (parkingspots != null && parkingspots.length == 0))
            return null;

        parkingspots = _.sortBy(parkingspots,'guid');
        return {"parkingspots":parkingspots};
    },
	
	getNeighbourcenter:function(){
        var neighbourcenter = this.AppData.neighbourcenter;

        if(neighbourcenter == null || (neighbourcenter != null && neighbourcenter.length == 0))
            return null;

        neighbourcenter = _.sortBy(neighbourcenter,'guid');
        return {"neighbourcenter":neighbourcenter};
    },
	
	getLibraries:function(){
        var libraries = this.AppData.libraries;

        if(libraries == null || (libraries != null && libraries.length == 0))
            return null;

        libraries = _.sortBy(libraries,'guid');
        return {"libraries":libraries};
    },
	
	getMovieTheatres:function(){
        var movietheatres = this.AppData.movietheatres;

        if(movietheatres == null || (movietheatres != null && movietheatres.length == 0))
            return null;

        movietheatres = _.sortBy(movietheatres,'guid');
        return {"movietheatres":movietheatres};
    },
	
	getForests:function(){
        var forests = this.AppData.forests;

        if(forests == null || (forests != null && forests.length == 0))
            return null;

        forests = _.sortBy(forests,'guid');
        return {"forests":forests};
    },
	
	getPlaystreets:function(){
        var playstreets = this.AppData.playstreets;

        if(playstreets == null || (playstreets != null && playstreets.length == 0))
            return null;

        forests = _.sortBy(playstreets,'guid');
        return {"playstreets":playstreets};
    },
	
//	getHospitals:function(){
//        var hospitals = this.AppData.hospitals;

//        if(hospitals == null || (hospitals != null && hospitals.length == 0))
//            return null;

//        forests = _.sortBy(hospitals,'guid');
//        return {"hospitals":hospitals};
//    },
	
//	getDoctors:function(){
//        var doctors = this.AppData.doctors;

//        if(doctors == null || (doctors != null && doctors.length == 0))
//            return null;

//        forests = _.sortBy(doctors,'guid');
//        return {"doctors":doctors};
//    },
	
//	getPharmacies:function(){
//        var pharmacies = this.AppData.pharmacies;

//        if(pharmacies == null || (pharmacies != null && pharmacies.length == 0))
//            return null;

//        forests = _.sortBy(pharmacies,'guid');
//        return {"pharmacies":pharmacies};
//    },
	
	
  //get..ById
    getRunningPlaceById:function(id){
        var place = _.find(this.AppData.runningplaces, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getParkingSpotById:function(id){
        var place = _.find(this.AppData.parkingspots, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getNeigbourCenterById:function(id){
        var place = _.find(this.AppData.neigbourcenter, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getLibrarieById:function(id){
        var place = _.find(this.AppData.libraries, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getMovieTheatreById:function(id){
        var place = _.find(this.AppData.movietheatres, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getForestById:function(id){
        var place = _.find(this.AppData.forests, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
	getPlayStreetsById:function(id){
        var place = _.find(this.AppData.playstreets, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
	
//	getHospitalById:function(id){
//        var place = _.find(this.AppData.hospitals, function(place){
//            return (place.guid == id);
//        });//Find an entity by id

//        if(typeof place == 'undefined')
//           return null;//Make null <-- undefined object

//        return place;
//   },
	
//	getDoctorById:function(id){
//        var place = _.find(this.AppData.doctors, function(place){
//            return (place.guid == id);
//        });//Find an entity by id

//        if(typeof place == 'undefined')
//            return null;//Make null <-- undefined object

//        return place;
//    },
	
	//getPharmaciesById:function(id){
//        var place = _.find(this.AppData.pharmacies, function(place){
//            return (place.guid == id);
//        });//Find an entity by id

//        if(typeof place == 'undefined')
//            return null;//Make null <-- undefined object

//        return place;
//    },
   
   
   //add..
    addRunningPlace:function(place){

        place.guid = Utils.guid();

        if(this.getRunningPlaceById(place.guid) != null)
            return 0;

        this.AppData.runningplaces.push(place);
        this.save();

        return 1;
    },
	
	addParkingSpot:function(place){

        place.guid = Utils.guid();

        if(this.getParkingSpotById(place.guid) != null)
            return 0;

        this.AppData.parkingspots.push(place);
        this.save();

        return 1;
    },
	
	addNeighbourCenter:function(place){

        place.guid = Utils.guid();

        if(this.getNeighbourCenterById(place.guid) != null)
            return 0;

        this.AppData.neighbourcenter.push(place);
        this.save();

        return 1;
    },
	
	addLibrarie:function(place){

        place.guid = Utils.guid();

        if(this.getLibrarieById(place.guid) != null)
            return 0;

        this.AppData.libraries.push(place);
        this.save();

        return 1;
    },
	
	addMovieTheatre:function(place){

        place.guid = Utils.guid();

        if(this.getMovieTheatreById(place.guid) != null)
            return 0;

        this.AppData.movietheatres.push(place);
        this.save();

        return 1;
    },
	
	addForest:function(place){

        place.guid = Utils.guid();

        if(this.getForestById(place.guid) != null)
            return 0;

        this.AppData.forests.push(place);
        this.save();

        return 1;
    },
	
	addPlaystreet:function(place){

        place.guid = Utils.guid();

        if(this.getPlaystreetById(place.guid) != null)
            return 0;

        this.AppData.playstreets.push(place);
        this.save();

        return 1;
    },
	
//	addHospital:function(place){

//        place.guid = Utils.guid();

//        if(this.getHospitalById(place.guid) != null)
//            return 0;

//        this.AppData.hospitals.push(place);
//        this.save();

//        return 1;
//    },
	
	//addDoctor:function(place){

//        place.guid = Utils.guid();

//        if(this.getDoctorById(place.guid) != null)
//            return 0;

//        this.AppData.doctors.push(place);
//        this.save();

//        return 1;
//    },
	
//		addPharmacie:function(place){

//        place.guid = Utils.guid();

//        if(this.getPharmacieById(place.guid) != null)
//            return 0;

//        this.AppData.pharmacies.push(place);
//        this.save();

//        return 1;
//    },
	
	
	
	
   //save function
    save:function(){
        this.AppData.modified = new Date().getTime();
        Utils.store(this.connString, this.AppData);//Save all data to the localstorage
    }
};

//GOOGLEMAPS
var GMap = {
    init:function(container){
        var mapOptions = {
            zoom:13,
            center: new google.maps.LatLng(51.048017, 3.727666)
        }
        this.map = new google.maps.Map(document.querySelector('#' + container), mapOptions);
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this.map,'resize');
        this.geoLocationMarker = null;
        this.markersVets = [];
        this.markersDogsRunningPlaces = [];
        this.markersDogsToilets = [];
    },
    addMarkerGeoLocation:function(geoLocation){
        this.geoLocationMarker = new google.maps.Marker({
            position:new google.maps.LatLng(geoLocation.coords.latitude, geoLocation.coords.longitude),
            title:"Hier ben ik",
            icon:'content/images/maps_home.png'
        });//Create a Google Maps Marker

        this.geoLocationMarker.setMap(this.map);//Add Marker to Map
        this.map.setCenter(new google.maps.LatLng(geoLocation.coords.latitude, geoLocation.coords.longitude));//Set center of the map to my geolocation
    },
    
    addMarkersForRunningPlaces:function(places){
        var marker = null, self = this;

        _.each(places, function(place){
            marker = new google.maps.Marker({
                position:new google.maps.LatLng(place.lat, place.long),
                title:place.plaatsomsch,
                icon:'content/images/dogs_offleash.png'
            });//Create a Google Maps Marker

            marker.setMap(self.map);//Add Marker to Map
        });
    },
   
    showMarkersForPage:function(page){
        console.log(page);
        switch(page){

            case 'runningplaces':
                this.hideMarkers(this.markersVets, true);
                this.hideMarkers(this.markersRunningPlaces,false);
                this.hideMarkers(this.markersDogsToilets,true);
            break;
        }
    },
    hideMarkers:function(arrMarkers, hide){
        var self = this;

        _.each(arrMarkers, function(marker){
            if(hide){
                marker.setMap(null);
            }else{
                marker.setMap(self.map);
            }
        });
    },
    refresh:function(){
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this.map,'resize');
    }
};

