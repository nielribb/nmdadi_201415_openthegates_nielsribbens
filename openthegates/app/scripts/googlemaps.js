/**
 * Created by drdynscript on 26/11/14.
 */
(function(){
    var key = 'AIzaSyAKHyWk_kVAOOzL_yk19IgEiUNBlyZa8zo';//Eigen Key Gebruiken!!!

    //Load Google Maps Async
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp'
        + '&key=' + key
        + '&callback=initGoogleMaps';
    document.body.appendChild(script);

    this.initGoogleMaps = function(){
        this.googleMapsInitialized = true;
    };

})();