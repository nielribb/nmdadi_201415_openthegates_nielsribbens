Handlebars.registerHelper('wheretogoAddress', function(vet) {
    return wheretogo.adres.capitalize()
        + " "
        + wheretogo.huisnr
        + ", "
        + wheretogo.postcode
        + " "
        + wheretogo.gemeente.capitalize();
});

Handlebars.registerHelper('geoReadable', function(lat, lng){
    return '(' + parseFloat(lat).toFixed(5) + ', ' + parseFloat(lng).toFixed(5) + ')';
});

Handlebars.registerHelper('geoDistance', function(place, geoLocation){
    var distance = Utils.calculateDistanceBetweenTwoCoordinates(place.lat, place.long, geoLocation.coords.latitude, geoLocation.coords.longitude);

    if(distance > 1){
        distance = distance.toFixed(3) + ' km';
    }else{
        distance = (distance*1000).toFixed(0) + ' m';
    }

    return distance;
});

Handlebars.registerHelper('usageYear', function(year) {
    return (year > 1970)?' (Ingebruikname: '+year + ')':'';
});

Handlebars.registerHelper('getReadableGeoDistance', function(distance) {
    if(distance > 1){
        distance = distance.toFixed(3) + ' km';
    }else{
        distance = (distance*1000).toFixed(0) + ' m';
    }

    return distance;
});
