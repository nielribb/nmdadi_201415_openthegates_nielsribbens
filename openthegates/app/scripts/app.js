/*
* Created by: Niels Ribbens
* Date: 1-08-2015
* Name: app.js
* Description: Ghent - WHERETOGO application
* 1.  Losloopweides: http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json
* 2.  Parkeergarages: http://datatank.gent.be/Infrastructuur/Parkeergarages.json
* 3.  Buurtcentra: http://datatank.gent.be/Infrastructuur/Buurtcentra.json
* 4.  Bibliotheken: http://datatank4.gent.be/cultuursportvrijetijd/bibliotheek.json
* 5.  Bioscopen: http://datatank4.gent.be/cultuursportvrijetijd/bioscopen.json
* 6.  Parken: http://datatank4.gent.be/milieuennatuur/parken.json
* 7.  Speelstraten: http://datatank4.gent.be/cultuursportvrijetijd/speelstratenzomer2015.json
* 8.  Ziekenhuizen: http://data.appsforghent.be/poi/ziekenhuizen.json
* 9.  Huisartsen: http://data.appsforghent.be/poi/huisartsenwachtposten.json
* 10. Apotheken: http://data.appsforghent.be/poi/apotheken.json
*
*/
(function(){
    var App = {
        init:function(){
            //Create the API's via a clone
            this.hondenvoorzieningenAPI = HondenvoorzieningenAPI;
            this.hondenvoorzieningenAPI.init('http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json');//Initialize
			this.parkeergaragesAPI = ParkeergaragesAPI;
            this.parkeergaragesAPI.init('http://datatank.gent.be/Infrastructuur/Parkeergarages.json');//Initialize
            this.buurtcentraAPI = BuurtcentraAPI;
            this.buurtcentraAPI.init('http://datatank.gent.be/Infrastructuur/Buurtcentra.json');//Initialize
            this.bibliotheekAPI = BibliotheekAPI;
            this.bibliotheekAPI.init('http://datatank4.gent.be/cultuursportvrijetijd/bibliotheek.json');//Initialize
       		this.parkenAPI = ParkenAPI;
            this.parkenAPI.init('http://datatank4.gent.be/milieuennatuur/parken.json');//Initialize
            this.speelstratenzomer2015API = Speelstratenzomer2015API;
            this.speelstratenzomer2015API.init('http://datatank4.gent.be/cultuursportvrijetijd/speelstratenzomer2015.json');//Initialize
            //this.ziekenhuizenAPI = ZiekenhuizenAPI;
            //this.ziekenhuizenAPI.init('http://datatank4.gent.be/cultuursportvrijetijd/speelstratenzomer2015.json');//Initialize
            //this.huisartsenwachtpostenAPI = HuisartsenwachtpostenAPI;
            //this.huisartsenwachtpostenAPI.init('http://data.appsforghent.be/poi/huisartsenwachtposten.json');//Initialize
            //this.apothekenAPI = ApothekenAPI;
            //this.apothekenAPI.init('http://data.appsforghent.be/poi/huisartsenwachtposten.json');//Initialize
			
			
			
			
			//Create WheretogoDBContext object via a clone
            this.wheretogoDBContext = WheretogoDBContext;
            this.wheretogoDBContext.init('dds.ghent.wheretogo');//Initialize
            //Create Handlebars Cache for templates
            this.hbsCache = {};
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Geolocation
            this.geoLocation = null;

            //Check Google Maps Initialized
            this.gMap = null;
            this.checkGoogleMapsInitialized();

            //Get GEOLocation
            if(this.geoLocation == null){
                this.getGEOLocation();
            }

            this.getRunningPlacesFromApi();
            //Get Losloopweides
            if(this.wheretogoDBContext.getRunningPlaces() == null){
                this.getRunningPlacesFromApi();
                console.log("getting runningplaces");
            }
			
//			 this.getParkingSpotsFromApi();
//	            //Get Parkeergarages
//	            if(this.wheretogoDBContext.getParkingSpots() == null){
//	                this.getParkingSpotsFromApi();
//	                console.log("getting parkingspots");
//	            }
			
//				this.getNeighbourCentersFromApi();
//	            //Get Buurtcentra
//	            if(this.wheretogoDBContext.getNeighbourCenters() == null){
//	               this.getNeighbourCentersFromApi();
//	                console.log("getting neighbourcenters");
//	            }
			
//				this.getLibrariesFromApi();
//	            //Get Bibliotheken
//	            if(this.wheretogoDBContext.getLibraries() == null){
//	                this.getLibrariesFromApi();
//	                console.log("getting libraries");
//	            }
			
//				this.getMovieTheatresFromApi();
//	            //Get Bioscopen
//	            if(this.wheretogoDBContext.getMovieTheatres() == null){
//	                this.getMovieTheatresFromApi();
//	                console.log("getting movietheatres");
//	            }
			
//				this.getForestsFromApi();
//	            //Get Parken
//	           if(this.wheretogoDBContext.getForests() == null){
//	                this.getForestsFromApi();
//	                console.log("getting forests");
//	            }
			
//			this.getPlaystreetsFromApi();
//	            if(this.wheretogoDBContext.getPlaystreets() == null){
//	                this.getPlaystreetsFromApi();
//	                console.log("getting playstreets");
//	            }
			
			//this.getHospitalsFromApi();
            ////Get Ziekenhuizen
            //if(this.wheretogoDBContext.getHospitals() == null){
            //    this.getHospitalsFromApi();
            //    console.log("getting hospitals");
            //}
			
			//this.getDoctorsFromApi();
            ////Get dokters
            //if(this.wheretogoDBContext.getDoctors() == null){
            //    this.getDoctorsFromApi();
            //    console.log("getting doctors");
            //}
			
			//this.getPharmaciesFromApi();
            ////Get apotheken
            //if(this.wheretogoDBContext.getPharmacies() == null){
            //    this.getPharmaciesFromApi();
            //    console.log("getting pharmacies");
            //}
				
            //Render the interface
            this.render();
        },
        checkGoogleMapsInitialized:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            if(!window.googleMapsInitialized){
                window.setTimeout(function(){self.checkGoogleMapsInitialized()}, 1000);
            }else{
                this.gMap = GMap;//Clone
                this.gMap.init('gmap-canvas');
            }
        },
       cacheElements:function(){
            /* Cache Handlebars Templates
            * Losloopweides, ...
            */
            if(!this.hbsCache.runningplaces){
                var src = document.querySelector('#runningplaces-template').innerHTML;
                this.hbsCache.runningplaces = Handlebars.compile(src);
            }
            
			if(!this.hbsCache.parkingspots){
               var src = document.querySelector('#parkingspots-template').innerHTML;
               this.hbsCache.parkingspots = Handlebars.compile(src);
            }
            
			if(!this.hbsCache.neighbourcenter){
                var src = document.querySelector('#neighbourcenter-template').innerHTML;
                this.hbsCache.neighbourcenter = Handlebars.compile(src);
            }
			
            if(!this.hbsCache.libraries){
               var src = document.querySelector('#libraries-template').innerHTML;
               this.hbsCache.libraries = Handlebars.compile(src);
            }
			
			if(!this.hbsCache.movietheatres){
               var src = document.querySelector('#movietheatres-template').innerHTML;
               this.hbsCache.movietheatres = Handlebars.compile(src);
			   
            } if(!this.hbsCache.forests){
               var src = document.querySelector('#forests-template').innerHTML;
               this.hbsCache.forests = Handlebars.compile(src);
			   
            } if(!this.hbsCache.playstreets){
                var src = document.querySelector('#playstreets-template').innerHTML;
                this.hbsCache.playstreets = Handlebars.compile(src);
			
            } //if(!this.hbsCache['hospitals']){
            //    var src = document.querySelector('#hospitals-template').innerHTML;
            //    this.hbsCache['hospitals'] = Handlebars.compile(src);
			
            //} if(!this.hbsCache['doctors']){
            //    var src = document.querySelector('#doctors-template').innerHTML;
            //    this.hbsCache['doctors'] = Handlebars.compile(src);
			
            //} if(!this.hbsCache['pharmacies']){
            //    var src = document.querySelector('#pharmacies-template').innerHTML;
            //    this.hbsCache['pharmacies'] = Handlebars.compile(src);
            //}
            
			/* DOM Elements
            * Vets List, ...
            */
            this.runningplacesList =  document.querySelector('#runningplaces-list');
            this.parkingspotsList =  document.querySelector('#parkingspots-list');
            this.neighbourcentersList = document.querySelector('#neighbourcenter-list');
			this.librariesList = document.querySelector('#libraries-list');
			this.movietheatresList = document.querySelector('#movietheatres-list');
			this.forestsList = document.querySelector('#forests-list');
			this.playstreetsList = document.querySelector('#playstreets-list');
			//this.hospitalsList = document.querySelector('#hospitals-list');
			//this.doctorsList = document.querySelector('#doctors-list');
			//this.pharmaciesList = document.querySelector('#pharmacies-list');
			
        },
        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            _.each(this.offcanvasToggles, function(toggle){
                toggle.addEventListener("click", function(ev){
                    ev.preventDefault();

                    var navigation_reference_name = this.getAttribute("href");
                    var navigation = document.querySelector(navigation_reference_name);
                    document.body.classList.toggle('offcanvasactive');

                    return false;
                }, false);
            });
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('home');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('home');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);document.body.classList.remove('offcanvasactive');});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        setActivePage:function(section){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages != null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id == section){
                        page.classList.add('active');
                        var pageStatesNavigation = page.querySelector('.page-state-navigation');

                        if(pageStatesNavigation){
                            var as = pageStatesNavigation.querySelectorAll('ul>li>a');
                            _.each(as, function(a){
                                a.addEventListener('click', function(ev){
                                    ev.preventDefault();

                                    self.setActivePageState(this.parentNode.dataset.page, this.parentNode.dataset.state);

                                    return false;
                                });
                            });
                        }
                        //Move Google Maps to the page
                        var connector = page.querySelector('.gmap-connector');
                        if(connector != null){
                            //connector.appendChild(document.querySelector('#gmap-canvas'));
                            if(self.gMap != null){
                                self.gMap.showMarkersForPage(section);
                                self.gMap.refresh();
                            }
                        }


                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.offcanvas-navigation ul li a');
            if(navLinks != null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') == effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        },
        setActivePageState:function(page, state){
            var pageStates = document.querySelectorAll('#' + page + ' .page-state')
            if(pageStates != null && pageStates.length > 0){
                _.each(pageStates,function(pageState){
                    if(pageState.dataset.state == state){
                        pageState.classList.add('active');
                    }else{
                        pageState.classList.remove('active');
                    }
                });
            }

            var pageStatesNavigation = document.querySelectorAll('[data-page=' + page + ']');
            if(pageStatesNavigation != null && pageStatesNavigation.length > 0){
                _.each(pageStatesNavigation,function(pageStateNav){
                    if(pageStateNav.dataset.state == state){
                        pageStateNav.classList.add('active');
                    }else{
                        pageStateNav.classList.remove('active');
                    }
                });
            }
        },
        render:function(){
            
			//Render the running places
            this.renderRunningPlaces(this.wheretogoDBContext.getRunningPlaces());
			//Render the parkingspots
            this.renderParkingSpots(this.wheretogoDBContext.getParkingSpots());
			////Render the neighbourcenters
//	        this.renderNeighbourcenter(this.wheretogoDBContext.getNeighbourcenter());
            //Render the libraries
            this.renderLibraries(this.wheretogoDBContext.getLibraries());
			//Render the movietheatres
            this.renderMovieTheatres(this.wheretogoDBContext.getMovieTheatres());
			//Render the forests
            this.renderForests(this.wheretogoDBContext.getForests());
			//Render the playstreets
//	            this.renderPlayStreets(this.wheretogoDBContext.getPlayStreets());
			////Render the hospitals
            //this.renderHospitals(this.wheretogoDBContext.getHospitals());
			////Render the doctors
            //this.renderDoctors(this.wheretogoDBContext.getDoctors());
			////Render the pharmacies
            //this.renderPharmacies(this.wheretogoDBContext.getPharmacies());
        },
        renderRunningPlaces:function(places){
            if(places != null && this.runningplacesList != null){
                places.geoLocation = this.geoLocation;
                this.runningplacesList.innerHTML = this.hbsCache.runningplaces(places);

                if(this.gMap != null){
                    this.gMap.addMarkersForRunningPlaces(places.runningplaces);
                }
            }
        },
		
		renderParkingSpots:function(places){
            if(places != null && this.parkingspotsList != null){
                places.geoLocation = this.geoLocation;
                this.parkingspotsList.innerHTML = this.hbsCache.parkingspots(places);

                if(this.gMap != null){
                    this.gMap.addMarkersForParkingSpots(places.parkingspots);
                }
            }
        },
		 
		 renderNeighbourCenters:function(places){
            if(places != null && this.neighbourcentersList != null){
               places.geoLocation = this.geoLocation;
               this.neighbourcentersList.innerHTML = this.hbsCache.neighbourcenters(places);

               if(this.gMap != null){
              		this.gMap.addMarkersForNeighbourCenters(places.neighbourcenters);
                }
            }
        },
		 
		 renderLibraries:function(places){
            if(places != null && this.librariesList != null){
                places.geoLocation = this.geoLocation;
                this.librariesList.innerHTML = this.hbsCache.libraries(places);

                if(this.gMap != null){
                    this.gMap.addMarkersForLibraries(places.libraries);
                }
            }
        },
		
		 renderMovieTheatres:function(places){
           if(places != null && this.movietheatresList != null){
             this.movietheatresList.innerHTML = this.hbsCache.movietheatres(places);

               if(this.gMap != null){
                   this.gMap.addMarkersForMovieTheatres(places.movietheatres);
              }
            }
        },
	
		 renderForests:function(places){
            if(places != null && this.forestsList != null){
                places.geoLocation = this.geoLocation;
                this.forestsList.innerHTML = this.hbsCache.forests(places);

               if(this.gMap != null){
                    this.gMap.addMarkersForForests(places.forests);
                }
            }
        },
      	
	  renderPlayStreets:function(places){
            if(places != null && this.playstreetsList != null){
                places.geoLocation = this.geoLocation;
                this.playstreetsList.innerHTML = this.hbsCache.playstreets(places);

                if(this.gMap != null){
                    this.gMap.addMarkersForPlayStreets(places.playstreets);
                }
            }
        },
		
	//	renderHospitals:function(places){
     //       if(places != null && this.hospitalsList != null){
    //            places.geoLocation = this.geoLocation;
      //          this.hospitalsList.innerHTML = this.hbsCache.hospitals(places);
//
      //          if(this.gMap != null){
      //              this.gMap.addMarkersForHospitals(places.hospitals);
      //          }
      //      }
     //   },
		//renderDoctors:function(places){
        //    if(places != null && this.doctorsList != null){
        //        places.geoLocation = this.geoLocation;
        //        this.doctorsList.innerHTML = this.hbsCache.doctors(places);

        //        if(this.gMap != null){
        //            this.gMap.addMarkersForDoctors(places.doctors);
        //        }
        //    }
       // },
		//renderPharmacies:function(places){
       //     if(places != null && this.pharmaciesList != null){
       //         places.geoLocation = this.geoLocation;
       //         this.pharmaciesList.innerHTML = this.hbsCache.pharmacies(places);

        //        if(this.gMap != null){
        //            this.gMap.addMarkersForPharmacies(places.pharmacies);
        //        }
       //     }
       // },
		
        getRunningPlacesFromApi:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            this.hondenvoorzieningenAPI.getHondenvoorzieningen().then(
                function(data){
                    //Get the value of the key Hondenvoorzieningen
                    var results = data.Hondenvoorzieningen;
                    //Filter the results
                    results = _.filter(results, function(obj){
                        return obj.soort == 'Losloopweide'
                    });

                    // debugging
                    console.log(results);

                    //Loop through the objects within the results --> Array
                    _.each(results, function(obj){
                        self.wheretogoDBContext.addRunningPlace(obj);
                    });
                    //Render the dogs running places
                    self.renderRunningPlaces(self.wheretogoDBContext.getRunningPlaces());
                },
                function(result){
                    console.log(result);
                }
            );
        },
		    
        getGEOLocation:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            Utils.getGEOLocationByPromise().then(
                function(location){
                    self.geoLocation = location;
                    //Render the interface again
                    self.render();
                    //Add GeoLocation To The Map
                    if(self.gMap != null){
                        self.gMap.addMarkerGeoLocation(self.geoLocation);
                    }
                },
                function(error){
                    self.geoLocation = null;
                }
            );
        }
    };

    App.init();//Initialize the application
})();